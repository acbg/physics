import numpy as np
import sys
# access to all other packages in the upper directory
sys.path.append('../')
from physics.constants import ureg, Q_
import physics.constants as pc

##############
# Solid state
##############
def Vt(T):
    '''
    thermal voltage
    '''
    return pc.k * T / pc.q

def Eg (mat, T):
    '''
    from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        Eg0 = Q_(1.519,'eV')
        EgA = Q_(5.405e-4, 'eV/K')
        T0 = Q_(204, 'K')
    Eg = Eg0 - EgA * T**2 / (T + T0)

    return Eg

def El (mat, T):
    '''
    from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        El0 = Q_(1.815, 'eV')
        ElA = Q_(6.05e-4, 'eV/K')
        T0 = Q_(204, 'K')
    El =  El0 - ElA * T**2 / (T + T0)

    return El

def Ex (mat, T):
    '''
    from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        Ex0 = Q_(1.981, 'eV')
        ExA = Q_(4.6e-4, 'eV/K')
        T0 = Q_(204, 'K')
    Ex = Ex0 - ExA * T**2 / (T + T0)

    return Ex

def Nv (mat, T):
    '''
    Effective density of states in the valence band

    Parameters from IOFFE,
    http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        Tv = Q_(1.83e15, 'K**-1.5 cm**-3')
    Nv = Tv * T**1.5

    return Nv

def Nc (mat, T):
    '''
    Effective density of states in the conduction band

    Nc= 8.63·1013·T3/2 [1-1.9310-4·T-4.19·10-8·T2 +21·exp(-EΓL/(2kbT)) +44·exp(-EΓX/(2kbT)) (cm-3)

    Parameters from IOFFE,
    http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        Tc0  = Q_(8.63e13, 'K**-1.5')
        Tc1 = Q_(1, 'cm**-3')
        Tc2 = Q_(1.931e-4, 'K**-1 cm**-3')
        Tc3 = Q_(4.190e-8, 'K**-2 cm**-3')*T**2
        Tc4 = Q_(21,'cm**-3')
        Tc5 = Q_(44,'cm**-3')

    c1 = pc.q * (Eg(mat,T) - El(mat,T)) / 2 / pc.k / T
    c1 = Tc4 * np.exp(c1.magnitude)
    c2 = pc.q * (Eg(mat,T) - Ex(mat,T)) / 2 / pc.k / T
    c2 = Tc5 * np.exp(c2.magnitude)
    Nc = Tc0 * T**(1.5) * (Tc1 - Tc2 * T - Tc3 * T**2 + c1 + c2)

    return Nc

def ni(mat, T):
    '''
    intrinsic carrier density
    '''
    if mat == 'GaAs':
        ni = Q_(2.1e6, 'cm**-3')
    else:
        ni = np.sqrt (Nc (mat,T) * Nv (mat,T)) * np.exp (-Eg (mat,T) / (2 * pc.k * T))

    return ni

def carrDensity (mat, u, T):
    a = ni (mat, T)
    a = Q_(2.1e6, 'cm**-3')
    b = np.exp (pc.q * u / 2 / pc.k / T)
    n = a * b
    return n

def hwave(mat, T):
    if mat == 'GaAs':
        Chw = 1.8
    hwave = Eg (mat, T) + 1.8 * pc.k * T / pc.q
    return hwave

def mobility (N, T, mat):
    a0 = mat.muMax * 300 * ureg.K * (300 * ureg.K / T) ** mat.theta1
    a =  a0 - mat.muMin
    b = 1 + (N / (mat.Nref * 300 *ureg.K * (T/(300*ureg.K))**mat.theta2))**mat.lamb
    mobility = mat.muMin +  a/b
    return mobility

##############
# Resistances
##############
def rWire (mobility, carrier_concent, length, crossSection):
    '''
    resistance of a thin film layer
    '''
    conductivity = pc.q * carrier_concent * mobility
    resistivity = 1 / conductivity
    resistance = resistivity * length / crossSection
    return resistance, conductivity, resistivity

def rSheet (mobility, carrierConcent, thickness):
    '''
    Sheet resistance of a layer
    '''
    conductivity = pc.q * carrierConcent * mobility
    resistivity = 1 / conductivity
    return resistivity / thickness

def rCirc (mobility, carrierConcent, thickness, rInt, rExt):
    '''
    thin-film layer resistance with circular geometry (Reeve's circular transmission model)
    '''
    rSh = rSheet (mobility, carrierConcent, thickness)
    rCirc= (rSh / 2 / np.pi) * (np.log (rExt.magnitude) - np.log (rInt.magnitude))\
            if rInt.magnitude != 0 \
            else (rSh / 2 / np.pi) * np.log (rExt.magnitude)
    return rCirc.to(ureg.ohm)


########
# Diodes
########
# ABC model
def iABC (n, A, B, C, ARthickness, area):

    #  n = carrDensity (u, T)

    #  iSRH = pc.q * area * ARthickness * A * n
    #  iRad = pc.q * area * ARthickness * B * n**2
    #  iAug = pc.q * area * ARthickness * C * n**3
    ni = Q_(2.1e6, 'cm**-3')
    iSRH = pc.q * area * ARthickness * A * (n**2 - ni**2) / (n - ni)
    iRad = pc.q * area * ARthickness * B * (n**2 - ni**2)
    iAug = pc.q * area * ARthickness * C * n * (n**2 - ni**2)
    i = iSRH + iRad + iAug

    iSRH = iSRH.to_base_units()
    iRad = iRad.to_base_units()
    iAug = iAug.to_base_units()
    i    = i.to_base_units()

    return i, iSRH, iRad, iAug

# Classical Shockley diode equation
def iShockley (u, iS, n, T):
    return iS * np.exp (pc.q * u / n / pc.k / T)

