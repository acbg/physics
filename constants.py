import numpy as np
import pathlib
import pint

# Create a units registry for all the scripts which inherit this constants file with
#     from constants import ureg, Q_, q, k

#  Set up a unit registry shorthand.
ureg = pint.UnitRegistry(system='mks')
ureg.setup_matplotlib(True)
#  ureg.load_definitions(str(pathlib.Path.home()) + '/.local/my_pint_defs.txt')
ureg.load_definitions('../physics/my_pint_defs.txt')
#  Set up a quantity shorthand.
Q_ = ureg.Quantity
#  Set up a PintArray shorthand.
#  PA_ = pint.PintArray
#  Operations between PintArrays of different unit registry will not work. We can change the unit registry that will be used in creating new PintArrays to prevent this issue.
#  pint.PintType.ureg = ureg

q = 1*ureg.elementary_charge
#  q = 1.6e-19 [C] [ampere*second]
#  print('q =',q.to_base_units())

k = 1*ureg.boltzmann_constant
#  k = 1.38e-23 # [J/K]
#  print('kB =',k.to_base_units())

#  print(Q_(1, 'elementary_charge').to_base_units())
#  print(Q_(1, 'electron_volt').to_base_units())
#  print(Q_(1, 'ohm').to_base_units())
